# CHANGELOG

The format is based on [Keep a Changelog](https://keepachangelog.com/), and this project adheres to [Semantic Versioning](https://semver.org/).

## [0.6.0] - 2023-07-14
### Added
- [@McFlyPL]: Added support for multiple transformations.

### Changed
- [@McFlyPL]: Changed API version to v4.
- [@McFlyPL]: Changed `setTransparentBlur` to `setBackgroundBlur`.
