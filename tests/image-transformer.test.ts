import {
    BackgroundBlurMap, BlurMap,
    encode,
    ImageTransformer,
    ParameterMap,
    ParametersMap,
    QualityMap,
    SizeMap,
    SizeTypeMap,
    TransformMap,
    TypeMap,
    UrlMap,
    WatermarkGravityMap,
    WatermarkMap
} from '../src';

describe('ImageTransformer', () => {
    const testImageUrl = 'https://test.pl/x.jpg';
    const clientId = 'd21daSA3rsa';
    const clientKey = 'sd1dqsrewfasf';

    it('get base url', () => {
        const url = (new ImageTransformer(testImageUrl, clientId, clientKey));
        const base64 = encode({
            [UrlMap.URL]: testImageUrl,
            [UrlMap.PARAMETERS]: {},
            [UrlMap.VERSION]: ImageTransformer.VERSION
        }, clientKey);

        const expectUrl = `https://i1.jagoda.cc/transforms/${clientId}/${base64}`;
        expect(url.getUrl()).toEqual(expectUrl);
        expect(String(url)).toEqual(expectUrl);
    });

    it('set parameters by methods', () => {
        const url = (new ImageTransformer(testImageUrl, clientId, clientKey))
            .setSize({ width: 200, height: 800, type: 'cover' })
            .setWatermark({
                url: 'https://jebac.pis/kaczor.png',
                height: 200,
                width: 100,
                gravity: 'center'
            })
            .setWatermark({
                url: 'https://konfa.ssie/ftw.png',
                height: 200,
                width: 100,
                gravity: 'west'
            });

        // @ts-expect-error
        const transforms = url.transforms;
        const expectParameters: ParametersMap = {
            [ParameterMap.TRANSFORMS]: [
                {
                    key: TransformMap.SIZE,
                    parameters: { [SizeMap.width]: 200, [SizeMap.height]: 800, [SizeMap.type]: SizeTypeMap.cover }
                },
                {
                    key: TransformMap.WATERMARK,
                    parameters: {
                        [WatermarkMap.url]: 'https://jebac.pis/kaczor.png',
                        [WatermarkMap.gravity]: WatermarkGravityMap.center,
                        [WatermarkMap.width]: 100,
                        [WatermarkMap.height]: 200
                    }
                },
                {
                    key: TransformMap.WATERMARK,
                    parameters: {
                        [WatermarkMap.url]: 'https://konfa.ssie/ftw.png',
                        [WatermarkMap.gravity]: WatermarkGravityMap.west,
                        [WatermarkMap.width]: 100,
                        [WatermarkMap.height]: 200
                    }
                }
            ]
        };
        const base64 = encode({
            [UrlMap.URL]: testImageUrl,
            [UrlMap.PARAMETERS]: expectParameters,
            [UrlMap.VERSION]: ImageTransformer.VERSION
        }, clientKey).replace('==', '');
        expect(transforms).toEqual(expectParameters[ParameterMap.TRANSFORMS]);
        expect(url.getUrl()).toEqual(`https://i1.jagoda.cc/transforms/${clientId}/${base64}`);
    });

    it('setSize()', () => {
        const heightParams = { height: 200 };
        const urlHeight = (new ImageTransformer(testImageUrl, clientId, clientKey)).setSize(heightParams);

        // @ts-expect-error
        expect(urlHeight.transforms[0]).toEqual({
            key: TransformMap.SIZE,
            parameters: {
                [SizeMap.height]: heightParams.height
            }
        });

        const widthParams = { width: 200 };
        const urlWidth = (new ImageTransformer(testImageUrl, clientId, clientKey)).setSize(widthParams);

        // @ts-expect-error
        expect(urlWidth.transforms[0]).toEqual({
            key: TransformMap.SIZE,
            parameters: {
                [SizeMap.width]: widthParams.width
            }
        });
    });

    it('set type resize without sizes', () => {
        const url = (new ImageTransformer(testImageUrl, clientId, clientKey)).setSize({ type: 'cover' });

        // @ts-expect-error
        const parameters = url.parameters;
        expect(parameters).toEqual({});
    });

    it('setType()', () => {
        const imageTransformer = (new ImageTransformer(testImageUrl, clientId, clientKey)).setType('jpg');

        // @ts-expect-error
        expect(imageTransformer.parameters).toEqual({ [ParameterMap.TYPE]: TypeMap.jpg });
    });

    it('setQuality()', () => {
        const imageTransformer = (new ImageTransformer(testImageUrl, clientId, clientKey)).setQuality('medium');

        // @ts-expect-error
        expect(imageTransformer.parameters).toEqual({ [ParameterMap.QUALITY]: QualityMap.medium });
    });

    it('setBlur()', () => {
        const imageTransformer = (new ImageTransformer(testImageUrl, clientId, clientKey)).setBlur(10);

        // @ts-expect-error
        expect(imageTransformer.transforms[0]).toEqual({
            key: TransformMap.BLUR,
            parameters: {
                [BlurMap.blur]: 10
            }
        });

        expect(() => {
            (new ImageTransformer(testImageUrl, clientId, clientKey)).setBlur(150);
        }).toThrowError('Blur value is must contain in 0 to 100.');
    });

    it('setBackgroundBlur()', () => {
        const width = 500;
        const height = 500;
        const blur = 50;
        const imageTransformer = (new ImageTransformer(testImageUrl, clientId, clientKey))
            .setBackgroundBlur(width, height, blur);

        // @ts-expect-error
        expect(imageTransformer.transforms[0]).toEqual(
            {
                key: TransformMap.BACKGROUND_BLUR,
                parameters: {
                    [BackgroundBlurMap.width]: width,
                    [BackgroundBlurMap.height]: height,
                    [BackgroundBlurMap.blur]: blur
                }
            }
        );

        expect(() => {
            (new ImageTransformer(testImageUrl, clientId, clientKey)).setBackgroundBlur(width, height, 150);
        }).toThrowError('Blur value is must contain in 0 to 100.');
    });

    it('get url with custom file name', () => {
        let url = (new ImageTransformer(testImageUrl, clientId, clientKey))
            .setFileName('test.jpg')
            .getUrl();
        let base64 = encode({
            [UrlMap.URL]: testImageUrl,
            [UrlMap.PARAMETERS]: {},
            [UrlMap.VERSION]: ImageTransformer.VERSION
        }, clientKey);
        expect(url).toEqual(`https://i1.jagoda.cc/transforms/${clientId}/test.jpg?transform=${base64}`);

        url = (new ImageTransformer(testImageUrl, clientId, clientKey))
            .setFileName('testowaNazwa', false)
            .getUrl();
        base64 = encode({
            [UrlMap.URL]: testImageUrl,
            [UrlMap.PARAMETERS]: {},
            [UrlMap.VERSION]: ImageTransformer.VERSION
        }, clientKey);
        expect(url).toEqual(`https://i1.jagoda.cc/transforms/${clientId}/testowaNazwa?transform=${base64}`);
    });
});
