import { encode } from '../src';

describe('encode', () => {
    it('should correct encode', () => {
        const key = 'dh8w9qhe21e12';
        const object = encode({
            test: 'object',
            with: {
                nodes: 'hello'
            }
        }, key);
        const hash = '2Sp7InRlc3QiOiJvYmplY3QiLCJ3aXRoIjp7Im5vZGVzIjoiaGVsbG8ifX030995e';

        expect(object).toEqual(hash);
    });
});
