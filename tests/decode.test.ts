import { decode } from '../src';
import { JagodaTransformerError } from '../src/errors/jagoda-transformer-error';

describe('decode', () => {
    it('should correct decode', () => {
        const object = {
            test: 'object',
            with: {
                nodes: 'hello'
            }
        };
        const hash = '2Sp7InRlc3QiOiJvYmplY3QiLCJ3aXRoIjp7Im5vZGVzIjoiaGVsbG8ifX030995e';

        expect(decode(hash, 'dh8w9qhe21e12')).toEqual(object);
    });

    it('should throw signature error', () => {
        const hash = '2Sp7InRlc3QiOiJvYmplY3QiLCJ3aXRoIjp7Im5vZGVzIjoiaGVsbG8ifX030995e';

        expect(() => decode(hash, 'bad')).toThrow(new JagodaTransformerError('Signature mishmash.'));
    });
});
