export enum UrlMap {
    VERSION = 0,
    URL = 1,
    PARAMETERS = 2
}

export enum ParameterV3Map {
    SIZE = 0,
    WATERMARK = 1,
    TYPE = 2,
    QUALITY = 3,
    BLUR = 4,
    TRANSPARENT_BLUR = 5
}

export enum ParameterMap {
    TYPE = 0,
    QUALITY = 1,
    TRANSFORMS = 2
}

export enum TransformMap {
    SIZE = 0,
    WATERMARK = 1,
    BLUR = 2,
    BACKGROUND_BLUR = 3
}

export enum SizeMap {
    'width' = 0,
    'height' = 1,
    'type' = 2
}

export enum SizeTypeMap {
    'contain' = 0,
    'cover' = 1,
    'fill' = 2,
    'inside' = 3,
    'outside' = 4
}

export enum QualityMap {
    'low' = 0,
    'medium' = 1,
    'high' = 2
}

export enum TypeMap {
    'jpg' = 0,
    'gif' = 1,
    'png' = 2,
    'webp' = 3
}

export enum WatermarkMap {
    'url' = 0,
    'width' = 1,
    'height' = 2,
    'gravity' = 3
}

export enum BlurMap {
    'blur' = 0
}

export enum BackgroundBlurMap {
    'width' = 0,
    'height' = 1,
    'blur' = 2
}

export enum WatermarkGravityMap {
    'north' = 0,
    'northeast' = 1,
    'southeast' = 2,
    'south' = 3,
    'southwest' = 4,
    'west' = 5,
    'northwest' = 6,
    'east' = 7,
    'center' = 8,
    'centre' = 9
}

interface TransformsMap {
    [TransformMap.SIZE]?: {
        [SizeMap.width]?: number;
        [SizeMap.height]?: number;
        [SizeMap.type]?: number;
    };
    [TransformMap.WATERMARK]?: {
        [WatermarkMap.url]: string;
        [WatermarkMap.height]?: number;
        [WatermarkMap.width]?: number;
        [WatermarkMap.gravity]?: WatermarkGravityMap;
    };
    [TransformMap.BACKGROUND_BLUR]?: {
        [BackgroundBlurMap.width]: number;
        [BackgroundBlurMap.height]: number;
        [BackgroundBlurMap.blur]: number;
    };
    [TransformMap.BLUR]?: {
        [BlurMap.blur]: number;
    };
}

export interface ParametersMap {
    [ParameterMap.TYPE]?: TypeMap;
    [ParameterMap.QUALITY]?: QualityMap;
    [ParameterMap.TRANSFORMS]?: TransformEntryMap<any>[]; // eslint-disable-line @typescript-eslint/no-explicit-any
}

export interface TransformEntryMap<T extends TransformMap> {
    key: T;
    parameters: NonNullable<TransformsMap[T]>;
}
