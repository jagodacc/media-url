import { QualityMap, SizeTypeMap, TypeMap, WatermarkGravityMap } from './maps';

export interface SizeParameters {
    width?: number;
    height?: number;
    type?: keyof typeof SizeTypeMap;
}

export interface WatermarkParameters {
    url: string;
    width?: number;
    height?: number;
    gravity?: keyof typeof WatermarkGravityMap;
}

export type TypeParameters = keyof typeof TypeMap;

export type QualityParameters = keyof typeof QualityMap;
