import slugify from 'slugify';
import { JagodaTransformerError } from './errors/jagoda-transformer-error';
import { QualityParameters, SizeParameters, TypeParameters, WatermarkParameters } from './interfaces/parameters';
import {
    ParameterMap,
    QualityMap,
    SizeMap,
    SizeTypeMap,
    TransformMap,
    BackgroundBlurMap,
    TypeMap,
    UrlMap,
    WatermarkGravityMap,
    WatermarkMap,
    BlurMap, ParametersMap, TransformEntryMap
} from './interfaces/maps';
import { encode } from './utils';

export class ImageTransformer {
    public static VERSION = '4';

    private static BASE_URL = 'https://i1.jagoda.cc';

    private readonly parameters: ParametersMap = {};

    private readonly transforms: TransformEntryMap<any>[] = []; // eslint-disable-line @typescript-eslint/no-explicit-any

    private fileName?: string;

    constructor(private url: string, private clientId: string, private clientKey: string) {
    }

    public setType(type: TypeParameters): this {
        this.parameters[ParameterMap.TYPE] = TypeMap[type];

        return this;
    }

    public setQuality(quality: QualityParameters): this {
        this.parameters[ParameterMap.QUALITY] = QualityMap[quality];

        return this;
    }

    public setSize({ width, height, type }: SizeParameters): this {
        width = Math.max(0, width ?? 0);
        height = Math.max(0, height ?? 0);

        if (!width && !height) {
            return this;
        }

        const transform: TransformEntryMap<TransformMap.SIZE> = {
            key: TransformMap.SIZE,
            parameters: {}
        };

        if (width) {
            transform.parameters[SizeMap.width] = width;
        }

        if (height) {
            transform.parameters[SizeMap.height] = height;
        }

        if (type) {
            transform.parameters[SizeMap.type] = SizeTypeMap[type];
        }

        this.transforms.push(transform);

        return this;
    }

    public setWatermark({ url, width, height, gravity }: WatermarkParameters): this {
        const transform: TransformEntryMap<TransformMap.WATERMARK> = {
            key: TransformMap.WATERMARK,
            parameters: {
                [WatermarkMap.url]: url
            }
        };

        if (gravity) {
            transform.parameters[WatermarkMap.gravity] = WatermarkGravityMap[gravity];
        }

        if (width) {
            transform.parameters[WatermarkMap.width] = width;
        }

        if (height) {
            transform.parameters[WatermarkMap.height] = height;
        }

        this.transforms.push(transform);

        return this;
    }

    public setBlur(blur: number): this {
        if (blur < 0 || blur > 100) {
            throw new JagodaTransformerError('Blur value is must contain in 0 to 100.');
        }

        const transform: TransformEntryMap<TransformMap.BLUR> = {
            key: TransformMap.BLUR,
            parameters: {
                [BlurMap.blur]: blur
            }
        };
        this.transforms.push(transform);

        return this;
    }

    public setBackgroundBlur(width: number, height: number, blur: number): this {
        if (blur < 0 || blur > 100) {
            throw new JagodaTransformerError('Blur value is must contain in 0 to 100.');
        }

        const transform: TransformEntryMap<TransformMap.BACKGROUND_BLUR> = {
            key: TransformMap.BACKGROUND_BLUR,
            parameters: {
                [BackgroundBlurMap.width]: width,
                [BackgroundBlurMap.height]: height,
                [BackgroundBlurMap.blur]: blur
            }
        };
        this.transforms.push(transform);

        return this;
    }

    public setFileName(fileName: string, slugifyName = true): this {
        this.fileName = slugifyName ? slugify(fileName) : fileName;

        return this;
    }

    public getUrl(): string {
        if (this.transforms.length) {
            this.parameters[ParameterMap.TRANSFORMS] = this.transforms;
        }

        const data = {
            [UrlMap.URL]: this.url,
            [UrlMap.PARAMETERS]: this.parameters,
            [UrlMap.VERSION]: ImageTransformer.VERSION
        };
        const base64 = encode(data, this.clientKey);

        if (this.fileName) {
            return `${ImageTransformer.BASE_URL}/transforms/${this.clientId}/${this.fileName}?transform=${base64}`;
        }

        return `${ImageTransformer.BASE_URL}/transforms/${this.clientId}/${base64}`;
    }

    public toString(): string {
        return this.getUrl();
    }
}
