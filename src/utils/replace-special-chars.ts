const SPECIAL_CHARS: {[key: string]: string;} = {
    '/': '!',
    '+': '$'
};

export function replaceSpecialChars(text: string, revert = false): string {
    for (const [search, replace] of Object.entries(SPECIAL_CHARS)) {
        if (revert) {
            const regex = `\\${replace}`;
            text = text.replace(new RegExp(regex, 'g'), search);
        } else {
            const regex = `\\${search}`;
            text = text.replace(new RegExp(regex, 'g'), replace);
        }
    }

    return text;
}
