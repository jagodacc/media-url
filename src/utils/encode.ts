import crypto from 'crypto';
import msgpack from 'msgpack5';
import { replaceSpecialChars } from './replace-special-chars';

export const SIGNATURE_LENGTH = 6;

export function calculateSignature(text: string, key: string): string {
    return crypto.createHash('md5').update(`${text}${key}`).digest('hex').slice(-1 * SIGNATURE_LENGTH);
}

export function encode(object: unknown, key: string): string {
    const json = JSON.stringify(object);
    const signature = calculateSignature(json, key);
    const base64Encode = msgpack().encode(json).toString('base64');

    return replaceSpecialChars(base64Encode).replace(/=/g, '') + signature;
}
