import msgpack from 'msgpack5';
import { JagodaTransformerError } from '../errors/jagoda-transformer-error';
import { calculateSignature, SIGNATURE_LENGTH } from './encode';
import { replaceSpecialChars } from './replace-special-chars';

export function decode(text: string, key: string): unknown {
    const hash = text.slice(0, -1 * SIGNATURE_LENGTH);
    const json = msgpack().decode(Buffer.from(replaceSpecialChars(hash, true), 'base64'));

    const signature = text.slice(-1 * SIGNATURE_LENGTH);
    const calculatedSignature = calculateSignature(json, key);

    if (signature !== calculatedSignature) {
        throw new JagodaTransformerError('Signature mishmash.');
    }

    return JSON.parse(json);
}
